
![alt text](https://gitlab.com/sketcher/domsway.js/raw/master/example/assets/img/apple-icon.png)

# What
DOMsway

## Why
Lightweight frontend javascript DOM manipulation inspired by jQuery

### How
Include DOMsway.js in you script tag

### Usage

Multiple queries in one go
```sh
_('button', 'a[href]', '*@textContent=Submit')
   .write('Click me!')
   .duplicate(true)        // Twice the amount of buttons now
   .style({backgroundColor:'blue'},{color:'white'})
   .write('Click us also, the blue buttons')
```

Traverse upwards in the DOM-tree
```sh
_('button')
   .traverse(-1)
   .write('Parent of a button')
```
List of Methods
```sh
_( [string || object]... )
   .traverse()
   .child()
   .parent()
   .add()
   .remove()
   .duplicate()
   .attr()
   .swarm()
   .class()
   .write()
   .event()
   .target()
   .throttle())
   .style()
   .form()
   .request()
   .debug()
   .get()
   .exists()
```

 
Documentation will be added at some point.